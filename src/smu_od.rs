#![allow(dead_code)]

use crate::config;
use std::{
    fmt,
    mem,
};

pub use config::SclkStep;

#[repr(u32)]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum SmuOdSclkIndex {
    Minimum = 0,
    Maximum = 1,
}

impl From<u32> for SmuOdSclkIndex {
    fn from(idx: u32) -> SmuOdSclkIndex {
        if !(0..=1).contains(&idx) {
            panic!("Invalid SmuOdSclkIndex: {}", idx);
        }
        unsafe { mem::transmute(idx) }
    }
}

#[repr(u32)]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum SmuOdMclkIndex {
    Maximum = 1,
}

impl From<u32> for SmuOdMclkIndex {
    fn from(idx: u32) -> SmuOdMclkIndex {
        if idx != 1 {
            panic!("Invalid SmuOdMclkIndex: {}", idx);
        }
        SmuOdMclkIndex::Maximum
    }
}

#[repr(u32)]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum SmuOdVcIndex {
    Minimum = 0,
    Midway = 1,
    Maximum = 2,
}

impl From<u32> for SmuOdVcIndex {
    fn from(idx: u32) -> SmuOdVcIndex {
        if !(0..=2).contains(&idx) {
            panic!("Invalid SmuOdVcIndex: {}", idx);
        }
        unsafe { mem::transmute(idx) }
    }
}

impl Into<SmuOdSclkIndex> for SmuOdVcIndex {
    fn into(self) -> SmuOdSclkIndex {
        use SmuOdVcIndex::*;
        match self {
            Minimum => SmuOdSclkIndex::Minimum,
            Maximum => SmuOdSclkIndex::Maximum,
            v => panic!("Invalid SmuOdSclkIndex: {:?}", &v),
        }
    }
}

#[derive(Debug)]
pub enum SmuOdCommand {
    SetSclk(Vec<(SmuOdSclkIndex, u32)>),
    SetMclk(Vec<(SmuOdMclkIndex, u32)>),
    SetVoltageCurve(SmuOdVcIndex, SclkStep),
    Commit,
    Restore,
}

impl fmt::Display for SmuOdCommand {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use SmuOdCommand::*;
        use fmt::Write;
        match self {
            SetSclk(v) => {
                let mut buffer = String::from("s");
                for &(idx, freq) in v.iter() {
                    write!(&mut buffer, " {} {}", idx as u32, freq)?;
                }
                write!(f, "{}", buffer)
            },
            SetMclk(v) => {
                let mut buffer = String::from("m");
                for &(idx, freq) in v.iter() {
                    write!(&mut buffer, " {} {}", idx as u32, freq)?;
                }
                write!(f, "{}", buffer)
            },
            SetVoltageCurve(idx, step) => {
                write!(f, "vc {} {} {}", *idx as u32, step.frequency(), step.voltage())
            },
            Commit => write!(f, "c"),
            Restore => write!(f, "r"),
        }
    }
}
