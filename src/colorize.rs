use std::{
    fmt::{
        self,
        Display,
    },
};
use termion::color::{
    self,
    Color,
};

#[derive(Clone, Copy)]
struct OptionDisplay<T: Sized>(pub Option<T>);

impl<T: Sized> Display for OptionDisplay<T> where
    T: Display,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match &self.0 {
            &Some(ref v) => write!(f, "{}", v),
            None => Ok(()),
        }
    }
}

// impl<'a, T: Sized> Display for OptionDisplay<&'a T> where
//     T: Display,
// {
//     fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
//         match *self {
//             Some(v) => write!(f, "{}", v),
//             None => Ok(()),
//         }
//     }
// }

pub struct Colorized<T: Sized, Fgc: Color, Bgc: Color> {
    fg: Option<color::Fg<Fgc>>,
    bg: Option<color::Bg<Bgc>>,
    v: T,
}

impl<T: Sized, Fgc: Color> Colorized<T, Fgc, color::Reset> {
    #[inline(always)]
    pub fn new_fg(v: T, fg: Fgc) -> Self {
        Self::new(v, Some(color::Fg(fg)), None)
    }
}

impl<T: Sized, Fgc: Color, Bgc: Color> Colorized<T, Fgc, Bgc> {
    #[inline(always)]
    pub fn new(v: T, fg: Option<color::Fg<Fgc>>, bg: Option<color::Bg<Bgc>>) -> Self {
        Colorized {
            fg: fg,
            bg: bg,
            v: v,
        }
    }

    fn fg_reset(&self) -> impl Display {
        let ret = self.fg.as_ref().map(|_| color::Fg(color::Reset));
        OptionDisplay(ret)
    }

    fn bg_reset(&self) -> impl Display {
        let ret = self.bg.as_ref().map(|_| color::Bg(color::Reset));
        OptionDisplay(ret)
    }
}

impl<T: Sized, Fgc: Color, Bgc: Color> Display for Colorized<T, Fgc, Bgc> where
    T: Display,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}{}{}{}{}", OptionDisplay(self.fg.as_ref()), OptionDisplay(self.bg.as_ref()), &self.v, self.fg_reset(), self.bg_reset())
    }
}
