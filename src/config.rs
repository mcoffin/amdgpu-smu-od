use serde::{
    Deserialize,
    Serialize,
};

use std::borrow::Cow;
use std::path::Path;
use std::{
    io,
    fmt,
    fs,
    error::Error,
};

#[derive(PartialEq, Eq, Clone, Copy, Serialize, Deserialize)]
pub struct SclkStep {
    frequency: u32,
    voltage: u32,
}

impl SclkStep {
    #[inline(always)]
    pub fn frequency(&self) -> u32 {
        self.frequency
    }

    #[inline(always)]
    pub fn voltage(&self) -> u32 {
        self.voltage
    }
}

impl fmt::Debug for SclkStep {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "SclkStep({}Hz @ {}mV)", self.frequency, self.voltage)
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Serialize, Deserialize)]
pub struct Config<'a> {
    card: Cow<'a, str>,
    pub pp_table: Option<Cow<'a, Path>>,
    pub power_limit: Option<u64>,
    pub sclk: Option<[SclkStep; 3]>,
    pub mclk: Option<u32>,
}

impl<'a> Config<'a> {
    pub fn from_file<'p, P>(file_path: P) -> Result<Self, ConfigErrorWithFile<P>> where
        P: 'p + AsRef<Path> + Copy + fmt::Debug,
    {
        let file = fs::OpenOptions::new()
            .read(true)
            .open(file_path)
            .map_err(ConfigError::from)
            .map_err(|e| ConfigErrorWithFile::new(file_path, e))?;
        serde_yaml::from_reader(file)
            .map_err(ConfigError::from)
            .map_err(|e| ConfigErrorWithFile::new(file_path, e))
    }

    pub fn card(&self) -> &str {
        &*self.card
    }
}

#[derive(Debug)]
pub struct ConfigErrorWithFile<P: AsRef<Path> + fmt::Debug> {
    path: P,
    error: ConfigError,
    description: String,
}

impl<P: AsRef<Path> + fmt::Debug> ConfigErrorWithFile<P> {
    pub fn new(path: P, err: ConfigError) -> Self {
        let description = format!("Error loading config file \"{}\"", path.as_ref().display());
        ConfigErrorWithFile {
            path: path,
            error: err,
            description: description,
        }
    }
}

impl<P: AsRef<Path> + fmt::Debug> fmt::Display for ConfigErrorWithFile<P> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let path = self.path.as_ref();
        write!(f, "Error in config file \"{}\": {}", path.display(), &self.error)
    }
}

impl<P: AsRef<Path> + fmt::Debug> Error for ConfigErrorWithFile<P> {
    fn description(&self) -> &str {
        self.description.as_str()
    }

    #[inline]
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        self.error.source()
    }
}

#[derive(Debug)]
pub enum ConfigError {
    Io(io::Error),
    Yaml(serde_yaml::Error),
}

impl ConfigError {
    const fn outer_description() -> &'static str {
        "Error loading config file"
    }
}

impl From<io::Error> for ConfigError {
    #[inline]
    fn from(e: io::Error) -> ConfigError {
        ConfigError::Io(e)
    }
}

impl From<serde_yaml::Error> for ConfigError {
    #[inline]
    fn from(e: serde_yaml::Error) -> ConfigError {
        ConfigError::Yaml(e)
    }
}

impl Error for ConfigError {
    fn description(&self) -> &str {
        "Error loading config file"
    }

    fn source(&self) -> Option<&(dyn Error + 'static)> {
        let src: &(dyn Error + 'static) = match self {
            &ConfigError::Io(ref e) => e,
            &ConfigError::Yaml(ref e) => e,
        };
        Some(src)
    }
}

impl fmt::Display for ConfigError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", Self::outer_description())?;

        let mut runner: &(dyn Error + 'static) = self;
        while let Some(source) = runner.source() {
            write!(f, ": {}", runner)?;
            runner = source;
        }
        Ok(())
    }
}
