#[macro_use] extern crate clap;
extern crate dirs;
extern crate env_logger;
extern crate log;
extern crate serde;
extern crate termion;

mod clap_util;
pub(crate) mod config;
pub(crate) mod colorize;
mod device;
mod smu_od;

use config::Config;
use device::AmdgpuDevice;
use log::{
    debug,
};
use std::{
    collections::LinkedList,
    sync,
};

fn app() -> clap::App<'static, 'static> {
    use clap::Arg;
    use clap_util::crate_app;
    let config_arg = Arg::with_name("config")
        .short("c")
        .long("config")
        .takes_value(true);
    let write_table_arg = Arg::with_name("write-table")
        .short("t")
        .long("write-table");
    crate_app()
        .arg(config_arg)
        .arg(write_table_arg)
}

static INIT_LOGGER: sync::Once = sync::Once::new();

trait OdDescription {
    fn get_od_commands(&self) -> LinkedList<smu_od::SmuOdCommand>;
}

impl<'a> OdDescription for Config<'a> {
    fn get_od_commands(&self) -> LinkedList<smu_od::SmuOdCommand> {
        use std::iter;
        use smu_od::*;
        let mut commands = LinkedList::new();
        if let Some(sclk_steps) = self.sclk.as_ref() {
            let sclk_steps = sclk_steps.iter()
                .map(|&v| v)
                .enumerate()
                .map(|(k, v)| (SmuOdVcIndex::from(k as u32), v))
                .filter(|&(idx, _)| idx != SmuOdVcIndex::Midway)
                .map(|(k, v)| (k.into(), v.frequency()));
            commands.push_back(SmuOdCommand::SetSclk(sclk_steps.collect()));
        }
        if let Some(mclk) = self.mclk {
            commands.push_back(SmuOdCommand::SetMclk(iter::once((SmuOdMclkIndex::Maximum, mclk)).collect()));
        }
        if let Some(sclk_steps) = self.sclk.as_ref() {
            let sclk_steps = sclk_steps.iter()
                .map(|&v| v)
                .enumerate()
                .map(|(k, v)| (SmuOdVcIndex::from(k as u32), v))
                .map(|(idx, v)| SmuOdCommand::SetVoltageCurve(idx, v));
            commands.extend(sclk_steps);
        }
        commands
    }
}

fn main() {
    INIT_LOGGER.call_once(|| env_logger::init());
    let matches = app().get_matches();
    let config = {
        use std::path::PathBuf;
        let config_filename = matches.value_of("config")
            .map(PathBuf::from)
            .unwrap_or_else(|| {
                let mut config_dir = dirs::config_dir().unwrap();
                config_dir.push("amdgpu-smu-od");
                config_dir.push("config.yml");
                config_dir
            });
        Config::from_file(&config_filename)
            .expect("Failed to read config file")
    };
    let mut device = AmdgpuDevice::new(config.card());
    debug!("{:?}", &config);
    let od_commands = config.get_od_commands();
    debug!("{:?}", &od_commands);
    if matches.is_present("write-table") {
        if let Some(pp_table) = config.pp_table.as_ref() {
            device.set_pptable(pp_table)
                .expect("Failed to write pp_table");
        }
    }
    if let Some(power_limit) = config.power_limit {
        device.set_power_limit(power_limit)
            .expect("Failed to set power limit");
    }
    device.od_commands(od_commands.iter())
        .expect("Failed to write od commands");
}
