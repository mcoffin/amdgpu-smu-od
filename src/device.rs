use log::{
    debug,
};
use std::{
    path::Path,
    path::PathBuf,
    io,
    fs,
};
use termion::color;
use crate::{
    colorize::Colorized,
    smu_od::SmuOdCommand,
};

pub struct AmdgpuDevice<P: AsRef<Path>> {
    path: P,
}

impl<P: AsRef<Path>> AmdgpuDevice<P> {
    #[inline]
    pub fn new(path: P) -> Self {
        AmdgpuDevice {
            path: path,
        }
    }

    pub fn set_power_limit(&mut self, power_limit: u64) -> io::Result<()> {
        use io::Write;
        let mut path = PathBuf::from(self.path.as_ref());
        path.push("hwmon");
        let mut hwmon_entry: Option<PathBuf> = None;
        for entry in fs::read_dir(&path)? {
            let entry = entry?;
            let ft = entry.file_type()?;
            if ft.is_dir() {
                hwmon_entry = Some(entry.path());
                break;
            }
        }
        let mut power_cap_path = hwmon_entry
            .map(Ok)
            .unwrap_or_else(|| Err(io::Error::new(io::ErrorKind::NotFound, "No such file or directory")))?;
        power_cap_path.push("power1_cap");
        debug!("Writing power cap ({}) to {}", power_limit, &Colorized::new_fg(power_cap_path.display(), color::Cyan));
        let mut power_cap_file = fs::OpenOptions::new()
            .write(true)
            .open(power_cap_path)?;
        write!(&mut power_cap_file, "{}", power_limit)?;
        Ok(())
    }

    pub fn set_pptable<W: AsRef<Path>>(&mut self, path: W) -> io::Result<()> {
        // TODO: actualy copy this file in rust but apparently thats multi-faceted
        use std::process::Command;
        let mut write_path = PathBuf::from(self.path.as_ref());
        write_path.push("pp_table");
        debug!("Writing pp_table from {} to {}", path.as_ref().display(), write_path.display());
        let status = Command::new("cp")
            .args(&[&PathBuf::from(path.as_ref()), &write_path])
            .status()?;
        if !status.success() {
            return Err(io::Error::new(io::ErrorKind::Other, "Failed to write pp_table"));
        }
        Ok(())
    }

    pub fn od_commands<'a, It>(&mut self, commands: It) -> io::Result<()> where
        It: Iterator<Item=&'a SmuOdCommand>,
    {
        use io::Write;
        let mut write_path = PathBuf::from(self.path.as_ref());
        write_path.push("pp_od_clk_voltage");

        debug!("Writing commands to {}{}{}", color::Fg(color::Cyan), write_path.display(), color::Fg(color::Reset));

        for cmd in commands {
            let mut od_file = fs::OpenOptions::new()
                .write(true)
                .open(&write_path)?;
            let buf = format!("{}\n", &cmd);
            debug!("Writing OD command: {:?}", &buf);
            od_file.write(buf.as_bytes())?;
            od_file.flush()?;
        }

        let mut od_file = fs::OpenOptions::new()
            .write(true)
            .open(write_path)?;
        let buf = format!("{}\n", SmuOdCommand::Commit);
        od_file.write(buf.as_bytes())?;
        od_file.flush()
    }
}
